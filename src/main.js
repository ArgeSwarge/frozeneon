import Vue from "vue";
import App from "./App.vue";

import VueI18n from "vue-i18n";
import en from "./i18n/languages/en.json";
import ru from "./i18n/languages/ru.json";

import "materialize-css/dist/js/materialize.min";

import router from "./router";

import store from "./store";

Vue.use(VueI18n);

Vue.config.productionTip = false;

export const eventBus = new Vue();

export const i18n = new VueI18n({
  locale: "en",
  fallbackLocale: "en",
  messages: {
    en,
    ru
  }
});

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount("#app");
