import Vue from "vue";
import Vuex from "vuex";
import packages from "@/store/packages";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    error: null
  },
  mutations: {
    SET_ERROR(state, error) {
      state.error = error;
    }
  },
  actions: {},
  getters: {
    error: s => s.error
  },
  modules: {
    packages
  }
});
