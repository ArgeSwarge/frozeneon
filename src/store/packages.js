export default {
  state: {
    packages: {
      list: [],
      pagination: {
        searchText: null,
        offset: 0,
        counter: 0,
        page: 0
      },
      loading: false
    }
  },
  mutations: {
    SET_PACKAGES_LIST(state, response) {
      state.packages.list.push(...response);
    },
    SET_PACKAGES_COUNTER(state, response) {
      state.packages.pagination.counter = response;
    },
    SET_PACKAGES_OFFSET(state, response) {
      state.packages.pagination.offset += response;
    },
    SET_PACKAGES_PAGE_COUNT(state, response) {
      state.packages.pagination.page += response;
    },
    RESET_PACKAGES_PAGINATION(state) {
      state.packages.list = [];
      state.packages.pagination.offset = 0;
      state.packages.pagination.counter = 0;
      state.packages.pagination.page = 0;
    },
    SET_LOADER_STATE(state, flag) {
      state.packages.loading = flag;
    }
  },
  actions: {
    async getPackagesList({ commit }, { offset, searchText, size }) {
      try {
        commit("SET_LOADER_STATE", true);
        fetch(
          `http://registry.npmjs.com/-/v1/search?text=${searchText}&size=${size}&from=${offset}&quality=1.0`
        )
          .then(response => {
            return response.json();
          })
          .then(data => {
            commit("SET_PACKAGES_LIST", data.objects);
            commit("SET_PACKAGES_COUNTER", data.total);
            commit("SET_PACKAGES_OFFSET", size);
            commit("SET_LOADER_STATE", false);
          });
      } catch (e) {
        commit("SET_ERROR");
      }
    },
    resetPackagesPagination({ commit }) {
      try {
        commit("RESET_PACKAGES_PAGINATION");
      } catch (e) {
        commit("SET_ERROR", e);
      }
    },
    setPackagesPaginationPage({ commit }, page) {
      try {
        commit("SET_PACKAGES_PAGE_COUNT", page);
      } catch (e) {
        commit("SET_ERROR", e);
      }
    }
  },
  getters: {
    packagesList: s => s.packages.list,
    packagesPagination: s => s.packages.pagination,
    packagesLoadingState: s => s.packages.loading
  }
};
